﻿using System;

namespace challenge.Models
{
    public class Compensation {
        public string EmployeeId { get; set; }
        public string CompensationId { get; set; }
        public int Salary { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}
