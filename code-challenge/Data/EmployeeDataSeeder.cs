﻿using challenge.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Data
{
    public class EmployeeDataSeederContent {
        public List<Employee> Employees { get; set; }
        public List<Compensation> Compensations { get; set; }
    }

    public class EmployeeDataSeeder
    {
        private EmployeeContext _employeeContext;
        private const String EMPLOYEE_SEED_DATA_FILE = "resources/EmployeeSeedData.json";

        public EmployeeDataSeeder(EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }

        public async Task Seed()
        {
            if(!_employeeContext.Employees.Any())
            {
                EmployeeDataSeederContent content = LoadContent();

                _employeeContext.Employees.AddRange(content.Employees);
                await _employeeContext.SaveChangesAsync();

            }
        }

        private EmployeeDataSeederContent LoadContent()
        {
            using (FileStream fs = new FileStream(EMPLOYEE_SEED_DATA_FILE, FileMode.Open))
            using (StreamReader sr = new StreamReader(fs))
            using (JsonReader jr = new JsonTextReader(sr))
            {
                JsonSerializer serializer = new JsonSerializer();

                EmployeeDataSeederContent content = serializer.Deserialize<EmployeeDataSeederContent>(jr);
                FixUpReferences(content);

                return content;
            }
        }

        private void FixUpReferences(EmployeeDataSeederContent content)
        {

            var employeeIdRefMap = from employee in content.Employees
                                select new { Id = employee.EmployeeId, EmployeeRef = employee };


            var compensationIdRefMap = from compensation in content.Compensations
                                   select new { Id = compensation.CompensationId, CompensationRef = compensation };

            content.Employees.ForEach(employee =>
            {
                
                if (employee.DirectReports != null)
                {
                    var referencedCompensations = new List<Compensation>(employee.Compensations.Count);
                    var referencedEmployees = new List<Employee>(employee.DirectReports.Count);
                    employee.DirectReports.ForEach(report =>
                    {
                        var referencedEmployee = employeeIdRefMap.First(e => e.Id == report.EmployeeId).EmployeeRef;
                        referencedEmployees.Add(referencedEmployee);
                    });

                    employee.Compensations.ForEach(compensation =>
                    {
                        var referencedCompensation = compensationIdRefMap.First(e => e.Id == compensation.CompensationId).CompensationRef;
                        referencedCompensation.EmployeeId = employee.EmployeeId;
                         referencedCompensations.Add(referencedCompensation);
                    });
                    employee.DirectReports = referencedEmployees;
                    employee.Compensations = referencedCompensations;
                }
            });

        }
    }
}
