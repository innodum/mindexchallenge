﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace challenge.Services
{
    public static class EmployeeServiceHelper
    {
        public static Dictionary<string, List<string>> BuildGraph(IEnumerable<string> employeeIds, string[][] directReporterInfo)
        {
            // Theoretically O(n)
            Dictionary<string, List<string>> graph = employeeIds
                .ToDictionary(id => id, _ => new List<string>());


            foreach (var info in directReporterInfo)
            {
                var directReporterId = info[0];
                var employeeId = info[1];
                // Theoretically O(1)
                if (!graph.ContainsKey(employeeId))
                {
                    // Exceptional case, unlikely for in memory collection
                    throw new Exception($"Employee {employeeId} not found");
                }

                var descendants = graph[employeeId] ?? new List<string>();
                descendants.Add(directReporterId);
                graph[employeeId] = descendants;

            }

            return graph;


        }


        public static Dictionary<string, int> GetReporterInfo(string targetEmployeeId, IEnumerable<string> employeeIds, string[][] directReporterInfo)
        {
            var graph = BuildGraph(employeeIds, directReporterInfo);
            var results = GetReporterInfo(targetEmployeeId, graph);
            return results;
        }

        public static Dictionary<string, int> GetReporterInfo(string targetEmployeeId, Dictionary<string, List<string>> graph)
        {
            var queue = new Queue<string>();
            Dictionary<string, int> reporterInfoLookup = new Dictionary<string, int>
            {
                // Initialize results lookup with root node
                { targetEmployeeId, 0 }
            };

            // Push the root node
            queue.Enqueue(targetEmployeeId);

            while (queue.Any())
            {

                var searchNode = queue.Dequeue();
                var descendantNodes = graph[searchNode];
                var distanceFromRoot = reporterInfoLookup[searchNode];

                foreach (var descendant in descendantNodes)
                {
                    var innerDescendantNodes = graph[descendant];
                    // If don't visit twice
                    if (!reporterInfoLookup.ContainsKey(descendant))
                    {
                        reporterInfoLookup.Add(descendant, distanceFromRoot + 1);
                        queue.Enqueue(descendant);
                    }
                }
            }

            // Exclude root node from result
            reporterInfoLookup.Remove(targetEmployeeId);

            return reporterInfoLookup;

        }
    }
}
