﻿using challenge.Models;
using System;

namespace challenge.Services
{
    public interface ICompensationService
    {
        Compensation GetById(String id);
        Compensation GetByEmployeeId(String employeeId);
        Compensation Create(Compensation employee); 

    }
}
