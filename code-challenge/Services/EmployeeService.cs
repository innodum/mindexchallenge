﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using challenge.Models;
using Microsoft.Extensions.Logging;
using challenge.Repositories;

namespace challenge.Services
{

    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILogger<EmployeeService> _logger;

        public EmployeeService(ILogger<EmployeeService> logger, IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
            _logger = logger;
        }

        public ReportingStructure GetReportingStructure(string id)
        {

            Employee employee = GetById(id);
            if (employee == null) throw new EmployeeNotFoundException(id);
            return GetReportingStructure(employee);
        }

        public ReportingStructure GetReportingStructure(Employee employee)
        {

            var employeeIds = _employeeRepository
                .GetQueryable()
                .Select(emp => emp.EmployeeId)
                .ToArray();


            var directReporterInfo = _employeeRepository
                    .GetQueryable()
                    .Where(emp => emp.DirectReports.Any())
                    .SelectMany(emp => emp.DirectReports.Select(directReporter => new string[]
                    /* DirectReporterInfo */
                    {
                       directReporter.EmployeeId,
                        emp.EmployeeId
                    })).ToArray();



            var reporterInfo = EmployeeServiceHelper.GetReporterInfo(employee.EmployeeId, employeeIds, directReporterInfo);

            var numberOfReports = reporterInfo.Count();

            return new ReportingStructure
            {
                EmployeeId = employee.EmployeeId,
                NumberOfReports = numberOfReports
            };

        }




        public Employee Create(Employee employee)
        {
            if (employee != null)
            {
                _employeeRepository.Add(employee);
                _employeeRepository.SaveAsync().Wait();
            }

            return employee;
        }

        public Employee GetById(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                return _employeeRepository.GetById(id);
            }

            return null;
        }

        public Employee Replace(Employee originalEmployee, Employee newEmployee)
        {
            if (originalEmployee != null)
            {
                _employeeRepository.Remove(originalEmployee);
                if (newEmployee != null)
                {
                    // ensure the original has been removed, otherwise EF will complain another entity w/ same id already exists
                    _employeeRepository.SaveAsync().Wait();

                    _employeeRepository.Add(newEmployee);
                    // overwrite the new id with previous employee id
                    newEmployee.EmployeeId = originalEmployee.EmployeeId;
                }
                _employeeRepository.SaveAsync().Wait();
            }

            return newEmployee;
        }
    }
}
