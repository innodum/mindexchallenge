﻿using challenge.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace challenge.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {

    }
}