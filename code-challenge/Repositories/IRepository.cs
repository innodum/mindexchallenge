﻿using System;
using System.Linq;
using System.Threading.Tasks;
using challenge.Models;
using Microsoft.Extensions.Logging;
using challenge.Data;

namespace challenge.Repositories
{
    public interface IRepository<TModel> {
        IQueryable<TModel> GetQueryable();
        TModel GetById(String id);
        TModel Add(TModel model);
        TModel Remove(TModel model);
        Task SaveAsync();
    }
}
