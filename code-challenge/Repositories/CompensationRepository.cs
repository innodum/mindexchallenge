﻿using System;
using System.Linq;
using System.Threading.Tasks;
using challenge.Data;
using challenge.Models;
using Microsoft.Extensions.Logging;

namespace challenge.Repositories
{
    public class CompensationRepository : ICompensationRepository
    {

        private readonly EmployeeContext _employeeContext;
        private readonly ILogger<ICompensationRepository> _logger;

        public CompensationRepository(ILogger<ICompensationRepository> logger, EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
            _logger = logger;
        }

        public IQueryable<Compensation> GetQueryable()
        {
            return _employeeContext.Compensations;
        }
        public Compensation Add(Compensation model)
        {

            model.CompensationId = Guid.NewGuid().ToString();
            _employeeContext.Compensations.Add(model);
            return model;
        }

        public Compensation GetById(string id)
        {
            return _employeeContext.Compensations.SingleOrDefault(e => e.CompensationId == id);

        }


        public IQueryable<Compensation> GetByEmployeeId(string employeeId)
        {
            return _employeeContext.Compensations.Where(e => e.EmployeeId == employeeId);

        }


        public Compensation Remove(Compensation model)
        {
            return _employeeContext.Remove(model).Entity;

        }

        public Task SaveAsync()
        {
            return _employeeContext.SaveChangesAsync();

        }
    }
}
