﻿using challenge.Controllers;
using challenge.Data;
using challenge.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using challenge.Tests.Integration.Extensions;

using System;
using System.IO;
using System.Net;
using System.Net.Http;
using challenge.Tests.Integration.Helpers;
using System.Text;
using System.Collections.Generic;
using challenge.Services;
using System.Linq;

namespace challenge.Tests.Integration
{
    [TestClass]
    public class EmployeeServiceHelperTests
    {

        [TestMethod]
        public void BuildGraph_Returns_Graph()
        {
            string[] employeeIds = new string[] {
                "John Lennon",
                 "Paul McCartney",
                 "Ringo Starr",
                 "Pete Best",
                 "George Harrison"

            };

            string[][] descendantInfo = new string[][] {
                new string[] {"Paul McCartney", "John Lennon"},
                new string[] {"Ringo Starr", "John Lennon"},
                new string[] {"Pete Best", "Ringo Starr"},
                new string[] {"George Harrison","Ringo Starr" },
            };


            var expected = new Dictionary<string, List<string>>{
                { "John Lennon",  new List<string>{ "Paul McCartney", "Ringo Starr" } },
                { "Paul McCartney",  new List<string>{ } },
                { "Ringo Starr",  new List<string>{ "Pete Best", "George Harrison" } },
                { "Pete Best",  new List<string>{  } },
                { "George Harrison",  new List<string>{ } }
            };

            var actual = EmployeeServiceHelper.BuildGraph(employeeIds, descendantInfo);

            Assert.AreEqual(expected.Count(), actual.Count());

            foreach (var expectedNode in expected)
            {
                var expectedDescendants = expectedNode.Value;
                var actualDescendants = actual[expectedNode.Key];

                expectedDescendants.Sort();
                actualDescendants.Sort();

                Assert.AreEqual(expectedDescendants.Count(), actualDescendants.Count());

                for (var i = 0; i < expectedDescendants.Count(); i++) {
                    var expectedDescenant = expectedDescendants[i];
                    var actualDescenant = actualDescendants[i];

                    Assert.AreEqual(expectedDescenant, actualDescenant);
                }

            }

        }

        [TestMethod]
        public void GetReporterInfo_Returns_ReporterInfo()
        {

            var expected = 5;

            // Arrange 
            var graph = new Dictionary<string, List<string>>{
                { "John Lennon",  new List<string>{ "Paul McCartney", "Ringo Starr" } },
                { "Paul McCartney",  new List<string>{ } },
                { "Ringo Starr",  new List<string>{ "Pete Best", "George Harrison" } },
                { "Pete Best",  new List<string>{  } },
                { "George Harrison",  new List<string>{ "Matt Smith" } },
                { "Matt Smith",  new List<string>{ } }
            };

            // Execute
            var results = EmployeeServiceHelper.GetReporterInfo("John Lennon", graph);
            var actual = results.Count();
            // Assert

            Assert.AreEqual(expected, actual);

        }
    }
}
